<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Test</title>
</head>
<body>
<c:out value="Index du serveur" />
<c:out value="${ 'a' < 'b' }" /> <%-- Affiche true --%>
<%-- Cette balise affichera le mot 'test' si le bean n'existe pas : --%>
<c:out value="${bean}">
    test
</c:out>
<%-- Elle peut également s'écrire sous cette forme : --%>
<c:out value="${bean}" default="test" />
</body>
</html>